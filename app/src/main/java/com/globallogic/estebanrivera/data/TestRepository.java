package com.globallogic.estebanrivera.data;

import com.globallogic.estebanrivera.data.di.Local;
import com.globallogic.estebanrivera.data.di.Remote;
import com.globallogic.estebanrivera.data.local.LocalDataPersistence;
import com.globallogic.estebanrivera.data.local.LocalDataSource;
import com.globallogic.estebanrivera.data.model.News;
import com.globallogic.estebanrivera.data.remote.TestRemoteDataSource;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class TestRepository{

    @Local
    private TestDataSource localDataSource;
    @Remote
    private TestDataSource remoteDataSource;
    @Local
    private LocalDataPersistence localDataPersistence;

    private LinkedHashMap<String, News> inMemoryNewsHomeDataSource = new LinkedHashMap<String, News>();

    private boolean forceRefresh = false;

    @Inject
    public TestRepository(LocalDataSource localDataSource, TestRemoteDataSource remoteDataSource, LocalDataPersistence localDataPersistence) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
        this.localDataPersistence = localDataPersistence;
    }

    public Observable<List<News>> getNews() {
        if(! forceRefresh && !inMemoryNewsHomeDataSource.isEmpty()){
            return getInMemoryNews();
        }

        Observable<List<News>> remoteNews = getAndSaveRemoteNews();

        if(forceRefresh){
            forceRefresh = false;
            return remoteNews;
        }
        else {
            ArrayList<Observable<List<News>>> sourcesObservable = new ArrayList<Observable<List<News>>>();

            if(remoteNews != null){
                sourcesObservable.add(remoteNews);
            }

            return Observable.concatDelayError(sourcesObservable)
                    .filter(News -> !News.isEmpty())
                    .firstElement().toObservable();
        }
    }

    // remote
    private Observable<List<News>> newsRemote() {
        try {
            return remoteDataSource.getNews();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Observable<List<News>> getAndSaveRemoteNews() {
        try {

            return remoteDataSource.getNews()
                    .doOnNext(  items ->{
                        for (News item : items) {
                            localDataPersistence.saveNews(item);

                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    // In memory
    private Observable<List<News>> getInMemoryNews() {
        List inMemoryData = new ArrayList(inMemoryNewsHomeDataSource.values());
        return Observable.just((inMemoryData));
    }
}
