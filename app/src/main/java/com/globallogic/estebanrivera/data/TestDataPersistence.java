package com.globallogic.estebanrivera.data;

import com.globallogic.estebanrivera.data.model.News;

public interface TestDataPersistence {
    void saveNews(News item);
}
