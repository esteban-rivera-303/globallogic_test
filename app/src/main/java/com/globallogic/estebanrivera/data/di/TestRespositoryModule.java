package com.globallogic.estebanrivera.data.di;

import com.globallogic.estebanrivera.data.TestDataPersistence;
import com.globallogic.estebanrivera.data.TestDataSource;
import com.globallogic.estebanrivera.data.local.LocalDataPersistence;
import com.globallogic.estebanrivera.data.local.LocalDataSource;
import com.globallogic.estebanrivera.data.remote.ApiService;
import com.globallogic.estebanrivera.data.remote.TestRemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TestRespositoryModule {

    @Provides
    @Singleton
    @Remote
    public TestDataSource provideRemoteDataSource(ApiService apiService){
        return new TestRemoteDataSource(apiService);
    }

    @Provides
    @Singleton
    @Local
    public LocalDataSource provideLocalDataSource() {
        return new LocalDataSource();
    }

    @Provides
    @Singleton
    @Local
    public TestDataPersistence provideLocalDataPersistence() {
        return new LocalDataPersistence();
    }
}
