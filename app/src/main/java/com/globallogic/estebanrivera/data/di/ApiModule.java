package com.globallogic.estebanrivera.data.di;

import com.globallogic.estebanrivera.common.DefaultSchedulerProvider;
import com.globallogic.estebanrivera.common.SchedulerProvider;
import com.globallogic.estebanrivera.data.remote.ApiService;
import com.globallogic.estebanrivera.util.Config;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public ApiService provideAPiService(){
        return new ApiService.Factory().create(Config.URL_API);
    }

    @Provides
    @Singleton
    public SchedulerProvider provideSchedulerProvider(){
        return new DefaultSchedulerProvider();
    }
}
