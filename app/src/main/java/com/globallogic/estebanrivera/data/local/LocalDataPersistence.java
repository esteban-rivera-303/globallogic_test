package com.globallogic.estebanrivera.data.local;

import com.globallogic.estebanrivera.data.TestDataPersistence;
import com.globallogic.estebanrivera.data.model.News;

import javax.inject.Inject;

public class LocalDataPersistence implements TestDataPersistence {

    @Inject
    public LocalDataPersistence() {
    }

    @Override
    public void saveNews(News item) {

    }
}
