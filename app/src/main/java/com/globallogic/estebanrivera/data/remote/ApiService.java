package com.globallogic.estebanrivera.data.remote;


import com.globallogic.estebanrivera.data.model.News;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * API service to interact with the API.
 */

public interface ApiService {

    @GET("list")
    Observable<List<News>> getListNews();

    /**
     * Convenience class to create API Service.
     */
    class Factory {
        public ApiService create(String base_url) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(ApiService.class);
        }
    }

}
