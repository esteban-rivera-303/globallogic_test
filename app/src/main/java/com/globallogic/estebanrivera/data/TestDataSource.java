package com.globallogic.estebanrivera.data;

import com.globallogic.estebanrivera.data.model.News;

import java.util.List;

import io.reactivex.Observable;

public interface TestDataSource {
    Observable<List<News>> getNews();
}
