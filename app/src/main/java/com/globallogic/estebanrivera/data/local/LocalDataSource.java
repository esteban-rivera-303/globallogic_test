package com.globallogic.estebanrivera.data.local;

import com.globallogic.estebanrivera.data.TestDataSource;
import com.globallogic.estebanrivera.data.model.News;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;


@Singleton
public class LocalDataSource implements TestDataSource {

    @Inject
    public LocalDataSource() {
    }

    @Override
    public Observable<List<News>> getNews() {
        return null;
    }
}
