package com.globallogic.estebanrivera.data.remote;

import com.globallogic.estebanrivera.data.TestDataSource;
import com.globallogic.estebanrivera.data.model.News;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class TestRemoteDataSource implements TestDataSource {

    private ApiService apiService;

    @Inject
    public TestRemoteDataSource(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Observable<List<News>> getNews() {
        return apiService.getListNews().map(dataResponse -> dataResponse);
    }
}
