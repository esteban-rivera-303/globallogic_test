package com.globallogic.estebanrivera.presenter.news.di;

import com.globallogic.estebanrivera.data.di.ScreenScoped;
import com.globallogic.estebanrivera.presenter.news.NewsFragment;

import dagger.Subcomponent;

@ScreenScoped
@Subcomponent(modules = {NewsPresenterModule.class})
public interface NewsComponent {

    void inject(NewsFragment fragment);
}
