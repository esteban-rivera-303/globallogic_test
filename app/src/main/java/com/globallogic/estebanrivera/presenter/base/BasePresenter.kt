package com.globallogic.estebanrivera.presenter.base

interface BasePresenter<V> {

    fun onAttach(view: V)

    fun onDetach()

}
