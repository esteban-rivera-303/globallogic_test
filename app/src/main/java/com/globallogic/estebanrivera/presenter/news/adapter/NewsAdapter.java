package com.globallogic.estebanrivera.presenter.news.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.globallogic.estebanrivera.R;
import com.globallogic.estebanrivera.data.model.News;
import com.globallogic.estebanrivera.databinding.NewsItemBinding;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsViewHolder> {

    private ItemCLickListener itemCLickListener;
    List<News> list = null;

    public NewsAdapter(ItemCLickListener itemCLickListener) {
        this.itemCLickListener = itemCLickListener;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        NewsItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_row_news, parent, false);
        return new NewsViewHolder(null, binding );
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        News item = list.get(position);
        if (item != null) {
            holder.bind(item, itemCLickListener);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ItemCLickListener {
        void onItemClicked(News item);
    }

    public void setDataList(List<News> list) {
        this.list = list;

    }

}
