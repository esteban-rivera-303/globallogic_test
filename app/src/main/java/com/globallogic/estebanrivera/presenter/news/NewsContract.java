package com.globallogic.estebanrivera.presenter.news;

import com.globallogic.estebanrivera.data.model.News;
import com.globallogic.estebanrivera.presenter.base.BasePresenter;
import com.globallogic.estebanrivera.presenter.base.BaseView;

import java.util.List;

public interface NewsContract {

    interface Presenter extends BasePresenter<View> {

        void fetchNewsList(boolean showLoading);

    }

    interface View extends BaseView {

        void setLoading(boolean loading);

        void showNews(List <News> list);

        void showNewsMore(List<News> list);

        void showLoadingError( String error);
    }
}
