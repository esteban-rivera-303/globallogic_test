package com.globallogic.estebanrivera.presenter.news.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.globallogic.estebanrivera.R;
import com.globallogic.estebanrivera.databinding.NewsDetailsBinding;
import com.globallogic.estebanrivera.presenter.base.BaseFragment;

import java.util.Objects;

public class NewsDetailsFragment extends BaseFragment {

    private static String ARG_TITLE = "arg_title";
    private static String ARG_IMAGE = "arg_image";
    private static String ARG_EXCERT = "arg_excert";

    private String title = "";
    private String image = "";
    private String excerpt = "";

    private NewsDetailsBinding binding;


    public static NewsDetailsFragment newInstance(String title, String excert, String image) {

        Bundle args = new Bundle();

        NewsDetailsFragment fragment = new NewsDetailsFragment();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_IMAGE, image);
        args.putString(ARG_EXCERT, excert);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (savedInstanceState != null) {
            title = retrieveNewsTitle(savedInstanceState);
            image = retrieveImage(savedInstanceState);
            excerpt = retrieveExcert(savedInstanceState);
        } else {
            assert getArguments() != null;
            title = getArguments().getString(ARG_TITLE);
            image = getArguments().getString(ARG_IMAGE);
            excerpt = getArguments().getString(ARG_EXCERT);
        }


    }

    private void attachListener() {
        binding.icBack.setOnClickListener(view -> {
                    Objects.requireNonNull(getActivity()).finish();
                    getActivity().overridePendingTransition(
                            R.anim.move_left_in_activity,
                            R.anim.move_right_out_activity
                    );
                }
        );
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_details, container, false);

        binding.title.setText(title);
        binding.excert.setText(excerpt);
        Glide.with(binding.image.getContext())
                .load((image))
                .placeholder(R.drawable.placeholder_color)
                .error(R.drawable.placeholder_color)
                .into(binding.image);

        attachListener();
        return binding.getRoot();
    }

    private String retrieveNewsTitle(Bundle savedInstanceState) {
        return savedInstanceState.getString(ARG_TITLE, "");
    }

    private String retrieveImage(Bundle savedInstanceState) {
        return savedInstanceState.getString(ARG_IMAGE, "");
    }

    private String retrieveExcert(Bundle savedInstanceState) {
        return savedInstanceState.getString(ARG_EXCERT, "");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_TITLE, title);
        outState.putString(ARG_IMAGE, image);
        outState.putString(ARG_EXCERT, excerpt);
    }


}
