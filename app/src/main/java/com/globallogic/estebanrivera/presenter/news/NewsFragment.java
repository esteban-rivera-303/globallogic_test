package com.globallogic.estebanrivera.presenter.news;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.globallogic.estebanrivera.App;
import com.globallogic.estebanrivera.R;
import com.globallogic.estebanrivera.data.model.News;
import com.globallogic.estebanrivera.databinding.NewsBinding;
import com.globallogic.estebanrivera.presenter.base.BaseFragment;
import com.globallogic.estebanrivera.presenter.news.adapter.NewsAdapter;
import com.globallogic.estebanrivera.presenter.news.details.NewsDetailsActivity;
import com.globallogic.estebanrivera.presenter.news.di.NewsPresenterModule;
import com.globallogic.estebanrivera.util.Util;

import java.util.List;

import javax.inject.Inject;

public class NewsFragment extends BaseFragment implements NewsContract.View, NewsAdapter.ItemCLickListener {

    private NewsBinding binding;
    private NewsAdapter adapter;

    @Inject
    NewsPresenter presenter;

    private GridLayoutManager linearLayoutManager;
    private List<News> newsList;
    private boolean isLoading = true;


    public static NewsFragment newInstance() {

        Bundle args = new Bundle();

        NewsFragment fragment = new NewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        injectComponents();
        presenter.onAttach(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_news,container, false);
        linearLayoutManager = new GridLayoutManager(getContext(), 1);
        attachListener();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.fetchNewsList(true);
    }

    void attachListener(){
        binding.retry.setOnClickListener(view -> presenter.fetchNewsList(true));
    }

    void injectComponents() {
        ((App) getActivity().getApplication()).getAppComponent().plus(new NewsPresenterModule(this)).inject(this);
    }

    @Override
    public void setLoading(boolean loading) {
        binding.progress.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNews(List<News> list) {
        if (!list.isEmpty()) {
            newsList = list;
            adapter = new NewsAdapter(this);
            adapter.setDataList(newsList);
            binding.recyclerview.setLayoutManager(linearLayoutManager);
            binding.recyclerview.setAdapter(adapter);
            binding.recyclerview.setVisibility(View.VISIBLE);
            binding.wrapperNoContent.setVisibility(View.GONE);
        } else {
            binding.wrapperNoContent.setVisibility(View.VISIBLE);
            binding.recyclerview.setVisibility(View.GONE);
        }

    }

    @Override
    public void showNewsMore(List<News> list) {

    }

    @Override
    public void showLoadingError(String error) {
        binding.wrapperNoContent.setVisibility(View.VISIBLE);
        binding.recyclerview.setVisibility(View.GONE);
    }

    @Override
    public void onItemClicked(News item) {

        Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);

        intent.putExtra("title", item.getTitle());
        intent.putExtra("image", Util.fullImage(item.getImage()));
        intent.putExtra("excerpt", item.getDescription());

        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_left, R.anim.fade_out);

    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            linearLayoutManager = new GridLayoutManager(getContext(), 1);
            binding.recyclerview.setLayoutManager(linearLayoutManager);
            adapter.notifyDataSetChanged();


        }else if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            linearLayoutManager = new GridLayoutManager(getContext(), 2);
            binding.recyclerview.setLayoutManager(linearLayoutManager);
            adapter.notifyDataSetChanged();

        }
    }
}
