package com.globallogic.estebanrivera.presenter.news.details;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.globallogic.estebanrivera.R;

public class NewsDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        String title = "", excerpt = "", image = "";
        if(getIntent() != null){
            if(getIntent().getStringExtra("title") != null)
                title = getIntent().getStringExtra("title");

            if(getIntent().getStringExtra("excerpt") != null)
                excerpt = getIntent().getStringExtra("excerpt");

            if(getIntent().getStringExtra("image") != null)
                image = getIntent().getStringExtra("image");
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.wrapper, NewsDetailsFragment.newInstance(title, excerpt, image))
                .commit();
    }
}
