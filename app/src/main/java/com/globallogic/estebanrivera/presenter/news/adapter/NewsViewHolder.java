package com.globallogic.estebanrivera.presenter.news.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Html;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.globallogic.estebanrivera.R;
import com.globallogic.estebanrivera.data.model.News;
import com.globallogic.estebanrivera.databinding.NewsItemBinding;
import com.globallogic.estebanrivera.util.Util;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;

public class NewsViewHolder extends RecyclerView.ViewHolder{

    private NewsItemBinding binding;


    public NewsViewHolder(@NonNull View itemView, NewsItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }


    public void bind(News item, NewsAdapter.ItemCLickListener itemCLickListener){
        binding.title.setText( Html.fromHtml(item.getTitle()));
        binding.excerpt.setText( Html.fromHtml(item.getDescription()));

        if (item.getImage() != null) {
            if(!item.getImage().isEmpty()){

/*                Picasso.get()
                        .load(item.getImage())
                        .resize(110, 110)
                        .into(binding.image);*/


                Glide.with(binding.image.getContext())
                        .load(Util.getThumbnail(item.getImage()))
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(binding.image);
            }
            else{
                binding.image.setImageResource(R.drawable.placeholder);
            }
        }
        else{
            binding.image.setImageResource(R.drawable.placeholder);
        }

        binding.getRoot().setOnClickListener(view -> {
            if(itemCLickListener != null){
                itemCLickListener.onItemClicked(item);
            }

        });

        binding.executePendingBindings();

    }

    private static class ResizeTransformation extends BitmapTransformation {

        private int width = 100, height = 100;

        public ResizeTransformation() {
        }

        public ResizeTransformation(int width, int height) {
            this.width = width;
            this.height = height;
        }

        protected Bitmap transform(BitmapPool bitmapPool, Bitmap original, int _width, int _height) {

            Bitmap result = bitmapPool.get(width, height, Bitmap.Config.ARGB_8888);
            // If no matching Bitmap is in the pool, get will return null, so we should allocate.
            if (result == null) {
                // Use ARGB_8888 since we're going to add alpha to the image.
                result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            }
            // Create a Canvas backed by the result Bitmap.
            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setAlpha(128);
            // Draw the original Bitmap onto the result Bitmap with a transformation.
            canvas.drawBitmap(original, 0, 0, paint);
            // Since we've replaced our original Bitmap, we return our new Bitmap here. Glide will
            // will take care of returning our original Bitmap to the BitmapPool for us.
            return result;
        }


        @Override
        public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

        }
    }
}

