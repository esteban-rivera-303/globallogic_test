package com.globallogic.estebanrivera.presenter.news.di;

import com.globallogic.estebanrivera.data.di.ScreenScoped;
import com.globallogic.estebanrivera.presenter.news.NewsContract;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsPresenterModule {
    private NewsContract.View view;

    public NewsPresenterModule(NewsContract.View view) {
        this.view = view;
    }

    @Provides
    @ScreenScoped
     NewsContract.View provideView(){
        return view;
    }
}
