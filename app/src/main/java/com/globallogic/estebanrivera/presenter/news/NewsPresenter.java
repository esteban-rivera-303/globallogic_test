package com.globallogic.estebanrivera.presenter.news;

import com.globallogic.estebanrivera.common.SchedulerProvider;
import com.globallogic.estebanrivera.data.TestRepository;
import com.globallogic.estebanrivera.data.model.News;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
/*import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;*/

public class NewsPresenter implements NewsContract.Presenter {

    private TestRepository testRepository;
    private SchedulerProvider schedulerProvider;

    //private CompositeDisposable compositeDisposable = null;
    private NewsContract.View view = null;

    @Inject
    NewsPresenter(TestRepository testRepository, SchedulerProvider schedulerProvider) {
        this.testRepository = testRepository;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void fetchNewsList(boolean showLoading) {
        view.setLoading(true);
        Observable<List<News>> disposable = testRepository.getNews();
        disposable.observeOn(schedulerProvider.mainThread())
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                        dataList -> {
                            view.showNews(dataList);
                            view.setLoading(false);

                        }
                        , throwable -> {
                            view.showLoadingError("Error");
                            view.setLoading(false);

                        }
                );
        //compositeDisposable.add( disposable);

    }

    @Override
    public void onAttach(NewsContract.View view) {
        this.view = view;
        //compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onDetach() {
        //compositeDisposable.clear();
    }
}
