package com.globallogic.estebanrivera.presenter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.globallogic.estebanrivera.R;
import com.globallogic.estebanrivera.presenter.news.NewsFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.wrapper, NewsFragment.newInstance())
                .commit();
    }
}
