package com.globallogic.estebanrivera;

import android.app.Application;

import com.globallogic.estebanrivera.di.AppComponent;
import com.globallogic.estebanrivera.di.AppModule;
import com.globallogic.estebanrivera.di.DaggerAppComponent;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        buildAppComponent();
    }

    private void buildAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private AppComponent appComponent = null;

}
