package com.globallogic.estebanrivera.util;

public class Util {

    public static String fullImage(String image) {
        int value = 0;
        String[] split = image.split("/");
        for (String item : split) {
            try {
                value = Integer.parseInt(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (value != 0) {
            return image.replace("" + value, "400");
        } else {
            return image;
        }
    }

    public static String getThumbnail(String image) {
        int value = 0;
        String[] split = image.split("/");
        for (String item : split) {
            try {
                value = Integer.parseInt(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (value != 0) {
            return image.replace("" + value, "100");
        } else {
            return image;
        }
    }
}
