package com.globallogic.estebanrivera.di;

import android.content.Context;

import com.globallogic.estebanrivera.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public App provideApplication(){
        return  app;
    }

    @Provides
    @Singleton
    public Context provideContext(){
        return app.getApplicationContext();
    }
}
