package com.globallogic.estebanrivera.di;

import com.globallogic.estebanrivera.data.di.ApiModule;
import com.globallogic.estebanrivera.data.di.TestRespositoryModule;
import com.globallogic.estebanrivera.presenter.news.di.NewsComponent;
import com.globallogic.estebanrivera.presenter.news.di.NewsPresenterModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {TestRespositoryModule.class, ApiModule.class, AppModule.class})
public interface AppComponent {

    NewsComponent plus(NewsPresenterModule module);

}
